# <b>MCP2515 driver internal</b>
## <b>Driver dependencies
The mcp2515 driver depends on the <i><b>Queue</b></i> sub-module implementation, the <i><b>Queue</b></i> sub-module offers some APIs needed to manage CAN Tx and Rx messages as well as dynamic creation and removal of queues, the needed interfaces will be mentioned later in this document.

To use the <i><b>Queue</b></i> sub-module correctly, the <b>MAX_QUEUE_NUMBER</b> should be set to 2 since the mcp2515 driver uses a queue for Tx message and a second one for Rx messages:

```C
#define VISIBILITY       static /**< Activate and deactivate the static attribute for unit test */

#define MAX_QUEUE_NUMBER (2)    /**< Maximum number of queues */
```
As per the VISIBILITY parameter, it's used for testing purpose to make the internal/static functions visible from outside the module.
The queue.h header file contains all the APIs declarations that can be used outside the queue module as well as a brief description for each function.

## <b>Driver initialization</b>

To be able to initialize the driver correctly, first the SPI driver should be initialized externally by the user application then the callback <i>cb_spi_write_bytes_gui8</i> should be set in the DeviceCfg_gst_t:
```c
typedef struct _DeviceCfg_gst_t {
  OperationMode_genm_t          operation_mode_enm;       /**< define the device mode of operation */
  SPIWriteBytesCB_gui8_t        cb_spi_write_bytes_gui8;  /**< SPI call back for read and write */
  MicroSecTimerCB_hui8_t        cb_time_stamp_gui8;       /**< time stamp call back */
  MsgTxConfirmationCB_hvd_t     cb_tx_confirmation_vd;    /**< TODO not implemented */
  MsgTxErrorIndicationCB_hvd_t  cb_tx_error_ind_vd;       /**< TODO not implemented */
  RXB0_ConfigSet_gst_t *        rx_buffer_0_cfg_pst;      /**< RXB0 configurations */
  RXB1_ConfigSet_gst_t *        rx_buffer_1_cfg_pst;      /**< RXB1 configurations */
  MsgsTxMode_genm_t             tx_mode_enm;              /**< Tx send mode (normal, one shot) */
} DeviceCfg_gst_t;
```
the device configuration structure contains other configuration parameters and callbacks that should be handled by the user before any CAN read or CAN write operation.
the sequence diagram below resumes how the initialization is performed, the blue messages are performed by the application user before calling the <i>device_init_gui8()</i> function:

![alt text](docs/diagrams/png/device-init-sequence.png)

## <b>MCP2515 driver interfaces</b>
TBD

![alt text](docs/diagrams/png/mcp2515_interfaces.png)
## <b>Main function</b>
TBD

![alt text](docs/diagrams/png/main_state_machine.png)


![alt text](docs/diagrams/png/read_state_flowchart.png)

![alt text](docs/diagrams/png/write_state_flowchart.png)
## <b>Send a CAN message</b>
TBD
## <b>Read a CAN message</b>
TBD