/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 * \file mcp2515_cfg.h
 *
 * \date Feb 16, 2020
 *      
 * \author jaguar
 * 
 * \brief Coniguration file for the mcp2515
 *
 */

#ifndef SRC_MCP2515_CFG_H_
#define SRC_MCP2515_CFG_H_

/*
 * C A N   B U S   C O N F I G U R A T I O N
 */

#define CAN_BUS_NBR_KHZ       (500) /**< */
#define EXT_MAIN_FOSC_KHZ     (8000) /**< */

/*

 */
#define TIME_QUANTUM_FACTOR (8)
/*-------------------------------------------------------------------------
 * Requirement:
 * ------------
 *  - TIME_QUANTUM_FACTOR x (RBP + 1) = (Fosc / ( 2 x Nbr))
 *-------------------------------------------------------------------------
 * Requirement (Datasheet page 43):
 * ---------------------------------
 *  - Propagation segment + PS1 >= PS2
 *  - Propagation segment + PS1 >= T_delay
 *  - PS2 > SJW
 *--------------------------------------------------------------------------
 * Requirement :
 * -------------
 *   +---------+--------------+---------------------+---------------------+
 *   | SyncSeg |    PropSeg   |   Phase Segment 1   |   Phase Segment 2   |
 *   +---------+--------------+---------------------+---------------------+
 *   \-------------------------------v------------------------------------/
 *                          TIME_QUANTUM_FACTOR
 *-------------------------------------------------------------------------
 */
#define SYNC_JUMP_WIDTH      (2)   /**< */
#define PROPAGATION_SEGMENT  (1)   /**< */
#define PHASE_SEGMENT_1      (3)   /**< */
#define SAMPLE_POINT_CONFIG  (1)   /**< */
#define PHASE_SEGMENT_2      (3)   /**< */

#if (PROPAGATION_SEGMENT + PHASE_SEGMENT_1 < PHASE_SEGMENT_2)
  #error "Config error: condition not met PROPAGATION_SEGMENT + PHASE_SEGMENT_1 >= PHASE_SEGMENT_2"
#endif

#if (PROPAGATION_SEGMENT + PHASE_SEGMENT_1 < 2)
#error "Config error: condition not met PROPAGATION_SEGMENT + PHASE_SEGMENT_1 >= 2"
#endif

#if (PHASE_SEGMENT_2 <= SAMPLE_POINT_CONFIG)
#error "Config error: condition not met PHASE_SEGMENT_1 > SAMPLE_POINT_CONFIG"
#endif

#if ((1 +  PROPAGATION_SEGMENT + PHASE_SEGMENT_1 + PHASE_SEGMENT_2) != TIME_QUANTUM_FACTOR)
  #error "Error: Nominal Bit Time doesn't use 16 time quantum !!!"
#endif

#define REG_BRP_VALUE ((EXT_MAIN_FOSC_KHZ / (2 * TIME_QUANTUM_FACTOR * CAN_BUS_NBR_KHZ)) - 1)

/*
 * O T H E R   C O N F I G U R A T I O N S
 */

#define TX_QUEUE_SIZE               (64)
#define RX_QUEUE_SIZE               (64)

#define ATOMIC_BEGIN_MAC()    __asm("nop")
#define ATOMIC_END_MAC()      __asm("nop")
#define TraceError_mac(error)         ESP_ERROR_CHECK(error)

#endif /* SRC_MCP2515_CFG_H_ */
