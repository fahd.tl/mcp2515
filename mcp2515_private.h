/*
 * mcp2515_private.h
 *
 *  Created on: Mar 21, 2020
 *      Author: jaguar
 */

#ifndef MAIN_MCP2515_PRIVATE_H_
#define MAIN_MCP2515_PRIVATE_H_

#include "mcp2515_cfg.h"
#include "mcp2515.h"
#include "queue.h"

#endif /* MAIN_MCP2515_PRIVATE_H_ */
