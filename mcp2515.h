/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 *
 * \file mcp2515.h
 *
 * \brief
 *
 * \date Feb 5, 2020
 *
 * \author Fahd Tauil (tauil.fahd@gmail.com)
 *
 */

#ifndef SRC_MCP2515_H_
#define SRC_MCP2515_H_

#include <stdint.h>
#include "std_types.h"

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*--------------------------[E X T E R N A L   T Y P E S   D E F I N I T I O N]------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/

#ifdef VISIBILITY
#undef VISIBILITY
#endif
#define VISIBILITY   // useful for module testing

#define TX_BUFFER_0 ((uint8_t)0x01)
#define TX_BUFFER_1 ((uint8_t)0x02)
#define TX_BUFFER_2 ((uint8_t)0x04)

#define RX_BUFFER_0 ((uint8_t)0x01)
#define RX_BUFFER_1 ((uint8_t)0x02)

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*---------------------------------[S P I   I N S T R U C T I O N S]-----------------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/
//General instructions
#define   SPI_RESET_INST                ((uint8_t)0xC0) /**< */
#define   SPI_READ_INST                 ((uint8_t)0x03) /**< */
#define   SPI_WRITE_INST                ((uint8_t)0x02) /**< */
#define   SPI_RTS_TXB0_INTS             ((uint8_t)0x81) /**< */
#define   SPI_RTS_TXB1_INTS             ((uint8_t)0x82) /**< */
#define   SPI_RTS_TXB2_INTS             ((uint8_t)0x84) /**< */
#define   SPI_BIT_MODIFY_INST           ((uint8_t)0x05) /**< */
#define   SPI_READ_STATUS_INST          ((uint8_t)0xA0) /**< */
#define   SPI_RX_STATUS_INST            ((uint8_t)0xB0) /**< */

//Rx buffer instructions
#define   SPI_READ_RXB0_HEADER_INST     ((uint8_t)0x90) /**< Read RX buffer to reduce the overhead of SPI */
#define   SPI_READ_RXB0_PAYLOAD_INST    ((uint8_t)0x92) /**< */
#define   SPI_READ_RXB1_HEADER_INST     ((uint8_t)0x94) /**< */
#define   SPI_READ_RXB1_PAYLOAD_INST    ((uint8_t)0x96) /**< */

//Tx buffer instructions
#define   SPI_LOAD_TXB0_ALL_INST        ((uint8_t)0x40) /**< */
#define   SPI_LOAD_TXB0_DATA_INST       ((uint8_t)0x41) /**< */
#define   SPI_LOAD_TXB1_ALL_INST        ((uint8_t)0x42) /**< */
#define   SPI_LOAD_TXB1_DATA_INST       ((uint8_t)0x43) /**< */
#define   SPI_LOAD_TXB2_ALL_INST        ((uint8_t)0x44) /**< */
#define   SPI_LOAD_TXB2_DATA_INST       ((uint8_t)0x45) /**< */

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
 *--------------------------[R X   S T A T U S   R E L A T E D   M A C R O S]--------------------------------*
 *-----------------------------------------------------------------------------------------------------------*
 \***********************************************************************************************************/
// Rx STATUS Instruction related definition
#define   RX_STATUS_NO_RX_MSG             ((uint8_t)0x00) /**< */
#define   RX_STATUS_MSG_IN_RXB0           ((uint8_t)0x01) /**< */
#define   RX_STATUS_MSG_IN_RXB1           ((uint8_t)0x02) /**< */
#define   RX_STATUS_MSG_IN_BOTH           ((uint8_t)0x03) /**< */

#define   RX_STATUS_STD_DATA_FRAME        ((uint8_t)0x00) /**< */
#define   RX_STATUS_STD_REMOTE_FRAME      ((uint8_t)0x01) /**< */
#define   RX_STATUS_EXT_DATA_FRAME        ((uint8_t)0x02) /**< */
#define   RX_STATUS_EXT_REMOTE_FRAME      ((uint8_t)0x03) /**< */

#define   RX_STATUS_RXF0_MATCH            ((uint8_t)0x00) /**< */
#define   RX_STATUS_RXF1_MATCH            ((uint8_t)0x01) /**< */
#define   RX_STATUS_RXF2_MATCH            ((uint8_t)0x02) /**< */
#define   RX_STATUS_RXF3_MATCH            ((uint8_t)0x03) /**< */
#define   RX_STATUS_RXF4_MATCH            ((uint8_t)0x04) /**< */
#define   RX_STATUS_RXF5_MATCH            ((uint8_t)0x05) /**< */
#define   RX_STATUS_RXF0_2_MATCH          ((uint8_t)0x06) /**< */
#define   RX_STATUS_RXF1_2_MATCH          ((uint8_t)0x07) /**< */

#define SPI_NO_ADDR ((uint8_t)0xFF)

typedef uint8_t   MsgPriority_gui8_t;
typedef uint8_t   TxBufferNbr_gui8_t;
typedef uint8_t   RxBufferNbr_gui8_t;
typedef uint32_t  TxTransactionID_gu32_t;

typedef enum _TxErrorType_genm_t {
  ERROR_CONDITION_enm = 0,
  ARBITRATION_LOST_enm
} TxErrorType_genm_t;

typedef struct _TxBuffersError_gst_t {
  TxErrorType_genm_t error_nature;
  TxBufferNbr_gui8_t tx_buffer_nbr;
} TxBuffersError_gst_t;

typedef enum _BitRate_genm_t {
  BITRATE_125K = 0,
  BITRATE_250K,
  BITRATE_500K,
  BITRATE_1M
} BitRate_genm_t;

typedef enum _FrameType_genm_t {
  DATA_FRAME_enm = 0,
  REMOTE_FRAME_enm
} FrameType_genm_t;

typedef enum _MsgsTxMode_genm_t {
  TX_NORMAL_MODE_enm = 0,
  TX_ONESHOT_MODE_enm,
  NUMBER_OF_TX_MODE_enm
} MsgsTxMode_genm_t;

typedef struct _CanMsg_gst_t {
  uint32_t msg_id_u32;                        /**< message id standard or extended */
  FrameType_genm_t frame_type_enm;            /**< frame type: remote or data */
  Boolean is_extd_id_bl;                      /**< flag indicate if the message is extended or standard */
  uint8_t msg_dlc_ui8;                        /**< length of the data within the frame */
  MsgPriority_gui8_t msg_priority_ui8;        /**< message priority 4 is high 0 is low */
  uint8_t data_pui8[ 8 ];                     /**< table for data */
} CanMsg_gst_t;

typedef struct _TimeStamp_gst_t {
  uint32_t time_stamp_hi_u32;                 /**< time stamp high resolution is 1 us */
  uint32_t time_stamp_lo_u32;                 /**< time stamp low resolution is 1 us */
} TimeStamp_gst_t;

typedef struct _CanContainer_gst_t {
  CanMsg_gst_t can_msg_data_st;               /**< Can message information */
  TimeStamp_gst_t time_stamp_st;              /**< To be filled by the application before pushed to queue*/
} CanContainer_gst_t;

typedef struct _TxCanContainer_gst_t {
  uint32_t trans_id_u32;                      /**< this ID will be used in case of transmission abort */
  CanContainer_gst_t can_container_st;        /**< Contains message data and time stamp */
} TxCanContainer_gst_t;

// Alias to keep the same naming, Rx frame don't need a transaction ID
typedef CanContainer_gst_t RxCanContainer_gst_t;

typedef enum _OperationMode_genm_t {
  NORMAL_MODE_enm = 0,
  SLEEP_MODE_enm,
  LOOPBACK_MODE_enm,
  LISTEN_ONLY_MODE_enm,
  CONFIG_MODE_enm,
  NUMBER_OF_OP_MODES_enm
} OperationMode_genm_t;

//RX0 buffer Acceptance mask and filters
typedef struct _RXB0_AcceptanceData_gst_t {
  uint32_t rx_mask_u32;                             /**< */
  uint32_t filters_pst[ 2 ];                        /**< */
} RXB0_AcceptanceData_gst_t;

//RX0 buffer Acceptance mask and filters
typedef struct _RXB1_AcceptanceData_gst_t {
  uint32_t rx_mask_u32;                             /**< */
  uint32_t filters_pst[ 4 ];                        /**< */
} RXB1_AcceptanceData_gst_t;

typedef struct _RXB0_ConfigSet_gst_t {
  RXB0_AcceptanceData_gst_t rxb0_accept_data_st;
  Boolean is_extended_bl;                           /**< */
  Boolean filters_masks_enable_bl;
  Boolean roll_over_enable_bl;
} RXB0_ConfigSet_gst_t;

typedef struct _RXB1_ConfigSet_gst_t {
  RXB1_AcceptanceData_gst_t rxb1_accept_data_st;
  Boolean is_extended_bl;                           /**< */
  Boolean filters_masks_enable_bl;
} RXB1_ConfigSet_gst_t;

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*-------------------------------[C A L L B A C K S   D E F I N I T I O N]-----------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/

// Callback for SPI write
typedef StdReturn_gui8_t (*SPIWriteBytesCB_gui8_t) ( uint8_t cmd_fui8, uint8_t addr_fui8, uint8_t * buffer_fpui8,
    uint32_t buffer_len_fu32 );

// Callback for Transmission confirmation
typedef void (*MsgTxConfirmationCB_hvd_t) ( TxBufferNbr_gui8_t buffers_nbr_fui8 );

// Callback for Transmission errors
typedef void (*MsgTxErrorIndicationCB_hvd_t) ( TxBuffersError_gst_t tx_buffers_error_fst );

// Callback for free running timer (tick is us)
typedef StdReturn_gui8_t (*MicroSecTimerCB_hui8_t) ( TimeStamp_gst_t * time_stamp_fpst );

typedef struct _DeviceCfg_gst_t {
  OperationMode_genm_t          operation_mode_enm;       /**< define the device mode of operation */
  SPIWriteBytesCB_gui8_t        cb_spi_write_bytes_gui8;  /**< SPI call back for read and write */
  MicroSecTimerCB_hui8_t        cb_time_stamp_gui8;       /**< time stamp call back */
  MsgTxConfirmationCB_hvd_t     cb_tx_confirmation_vd;    /**< TODO not implemented */
  MsgTxErrorIndicationCB_hvd_t  cb_tx_error_ind_vd;       /**< TODO not implemented */
  RXB0_ConfigSet_gst_t *        rx_buffer_0_cfg_pst;      /**< RXB0 configurations */
  RXB1_ConfigSet_gst_t *        rx_buffer_1_cfg_pst;      /**< RXB1 configurations */
  MsgsTxMode_genm_t             tx_mode_enm;              /**< Tx send mode (normal, one shot) */
} DeviceCfg_gst_t;

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*----------------------------------[E X T E R N A L   F U N C T I O N S]------------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/
/*
 * TODO Interrupt mode is not supported yet,the ESP32 CAN SHIELD HW use a pin for interrupt.
 * TODO Tx confirmation base on transaction id will be added later.
 * TODO Tx  error indication call back implementation will be added.
 * TODO Sleep mode full support will be added later.
 * TODO Make a gui to configure timing aspect for the BUS can, now the configuration is done manually in the
 *      mcp2515_cfg.h.
 *
 */

/**
 *
 * \brief
 *
 * \return
 *
 * \param
 *
 */
extern StdReturn_gui8_t device_init_gui8 ( const DeviceCfg_gst_t * device_cfg_fpst );
/**
 *
 * \brief
 *
 * \return
 *
 * \param
 *
 */
extern StdReturn_gui8_t abort_tx_buffers_gui8 ( TxBufferNbr_gui8_t buffers_fui8 );
/**
 *
 * \brief
 *
 * \return
 *
 * \param
 *
 */
extern StdReturn_gui8_t abort_all_tx_buffers_gui8 ( void );
/**
 *
 * \brief
 *
 * \return
 *
 * \param
 *
 */
extern StdReturn_gui8_t set_rx0_buffer_settings_gui8(const RXB0_ConfigSet_gst_t * rxb0_cfg_fpst);
/**
 *
 * \brief
 *
 * \return
 *
 * \param
 *
 */
extern StdReturn_gui8_t set_rx1_buffer_settings_gui8( const RXB1_ConfigSet_gst_t * rxb1_cfg_fpst);
/**
 *
 * \brief function used to switch to MCP2515 desired mode, it sets the mode and checks if mode is established
 *
 * \return OP_OK if the mode was switched correctly, OP_NOK if the mode was failed
 *
 * \param mode_of_op_fenm as input, holds the mode value
 *
 */
extern StdReturn_gui8_t set_operation_mode_gui8 ( OperationMode_genm_t mode_of_op_fenm );
/**
 *
 * \brief This function should be called cyclicly i.e in a periodic task, it holds the implementation of the
 *        main state machine. it handles send and receive requests done by read_can_msg_gui8 and write_can_msg_gui8
 *        functions.
 *
 * \note This function should be called cyclicly.
 *
 * \return void
 *
 * \param void
 *
 */
extern void mcp2515_main_gvd ( void );
/**
 *
 * \brief This function deinitialized the mcp2515 MCU and liberate the resources reserved for the Rx an Tx queues
 *
 * \return void
 *
 * \param void
 *
 */
extern StdReturn_gui8_t mcp2515_deinit_gui8 ( void );
/**
 *
 * \brief Reads the next message pushed to Rx queue.
 *
 * \return OP_OK if a message is received, OP_NOK if the queue is empty.
 *
 * \param can_msg_fpst as output, pointer to the structure that will hold the message attributes.
 *
 * \param queue_usage_fpu32 as output, pointer to a uint32 that will hold the Rx queue usage after pop operation.
 *
 */
extern StdReturn_gui8_t read_can_msg_gui8 ( RxCanContainer_gst_t * rx_can_msg_fpst, uint32_t * queue_usage_fpu32 );
/**
 *
 * \brief Write the message to be send to the queue, the transmission will be initiated by mcp2515 main function
 *
 * \return OP_OK if the Tx queue is not full, OP_NOK instead
 *
 * \param tx_can_msg_fpst as input/output, holds the message attributes the time stamp and the Tx transaction ID.
 *
 * \param queue_usage_fpu32 as output, pointer to a uint32 that will hold the Tx queue usage after push operation.
 *
 */
extern StdReturn_gui8_t write_can_msg_gui8 ( TxCanContainer_gst_t * tx_can_msg_fpst, uint32_t * queue_usage_fpu32 );


#endif /* SRC_MCP2515_H_ */
