/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 * @file std_types.h
 * @date Feb 5, 2020
 * @author Fahd Tauil (tauil.fahd@gmail.com)
 * @brief
 */

#ifndef SRC_STD_TYPES_H_
#define SRC_STD_TYPES_H_

#include <stdint.h>

#define NULL_PTR  ((void*)0)

#define TRUE      ((uint8_t)0x01)
#define FALSE     ((uint8_t)0x00)

#define OP_OK     ((uint8_t)0x00)
#define OP_NOK    ((uint8_t)0x01)

typedef uint8_t Boolean; /**< */

typedef uint8_t StdReturn_gui8_t; /**< */

#endif /* SRC_STD_TYPES_H_ */
