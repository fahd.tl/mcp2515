/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 *
 * \file mcp2515.c
 *
 * \date Feb 5, 2020
 *
 * \author Fahd Tauil (tauil.fahd@gmail.com)
 *
 * \brief
 *
 */

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*---------------------------------------------[I N C L U D E S]---------------------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "mcp2515_cfg.h"
#include "mcp2515.h"
#include "queue.h"
#include "errors_def.h"

#define REG_RXF0SIDH_ADDR    ((uint8_t)0x00)
#define REG_RXF0SIDL_ADDR    ((uint8_t)0x01)
#define REG_RXF0EID8_ADDR    ((uint8_t)0x02)
#define REG_RXF0EID0_ADDR    ((uint8_t)0x03)
#define REG_RXF1SIDH_ADDR    ((uint8_t)0x04)
#define REG_RXF1SIDL_ADDR    ((uint8_t)0x05)
#define REG_RXF1EID8_ADDR    ((uint8_t)0x06)
#define REG_RXF1EID0_ADDR    ((uint8_t)0x07)
#define REG_RXF2SIDH_ADDR    ((uint8_t)0x08)
#define REG_RXF2SIDL_ADDR    ((uint8_t)0x09)
#define REG_RXF2EID8_ADDR    ((uint8_t)0x0a)
#define REG_RXF2EID0_ADDR    ((uint8_t)0x0b)
#define REG_BFPCTRL_ADDR     ((uint8_t)0x0c)
#define REG_TXRTSCTRL_ADDR   ((uint8_t)0x0d)
#define REG_CANSTAT_ADDR     ((uint8_t)0x0e)
#define REG_CANCTRL_ADDR     ((uint8_t)0x0f)
#define REG_RXF3SIDH_ADDR    ((uint8_t)0x10)
#define REG_RXF3SIDL_ADDR    ((uint8_t)0x11)
#define REG_RXF3EID8_ADDR    ((uint8_t)0x12)
#define REG_RXF3EID0_ADDR    ((uint8_t)0x13)
#define REG_RXF4SIDH_ADDR    ((uint8_t)0x14)
#define REG_RXF4SIDL_ADDR    ((uint8_t)0x15)
#define REG_RXF4EID8_ADDR    ((uint8_t)0x16)
#define REG_RXF4EID0_ADDR    ((uint8_t)0x17)
#define REG_RXF5SIDH_ADDR    ((uint8_t)0x18)
#define REG_RXF5SIDL_ADDR    ((uint8_t)0x19)
#define REG_RXF5EID8_ADDR    ((uint8_t)0x1a)
#define REG_RXF5EID0_ADDR    ((uint8_t)0x1b)
#define REG_TEC_ADDR         ((uint8_t)0x1c)
#define REG_REC_ADDR         ((uint8_t)0x1d)
#define REG_RXM0SIDH_ADDR    ((uint8_t)0x20)
#define REG_RXM0SIDL_ADDR    ((uint8_t)0x21)
#define REG_RXM0EID8_ADDR    ((uint8_t)0x22)
#define REG_RXM0EID0_ADDR    ((uint8_t)0x23)
#define REG_RXM1SIDH_ADDR    ((uint8_t)0x24)
#define REG_RXM1SIDL_ADDR    ((uint8_t)0x25)
#define REG_RXM1EID8_ADDR    ((uint8_t)0x26)
#define REG_RXM1EID0_ADDR    ((uint8_t)0x27)
#define REG_CNF3_ADDR        ((uint8_t)0x28)
#define REG_CNF2_ADDR        ((uint8_t)0x29)
#define REG_CNF1_ADDR        ((uint8_t)0x2a)
#define REG_CANINTE_ADDR     ((uint8_t)0x2b)
#define REG_CANINTF_ADDR     ((uint8_t)0x2c)
#define REG_EFLG_ADDR        ((uint8_t)0x2d)
#define REG_TXB0CTRL_ADDR    ((uint8_t)0x30)
#define REG_TXB0SIDH_ADDR    ((uint8_t)0x31)
#define REG_TXB0SIDL_ADDR    ((uint8_t)0x32)
#define REG_TXB0EID8_ADDR    ((uint8_t)0x33)
#define REG_TXB0EID0_ADDR    ((uint8_t)0x34)
#define REG_TXB0DLC_ADDR     ((uint8_t)0x35)
#define REG_TXB0D0_ADDR      ((uint8_t)0x36)
#define REG_TXB0D1_ADDR      ((uint8_t)0x37)
#define REG_TXB0D2_ADDR      ((uint8_t)0x38)
#define REG_TXB0D3_ADDR      ((uint8_t)0x39)
#define REG_TXB0D4_ADDR      ((uint8_t)0x3a)
#define REG_TXB0D5_ADDR      ((uint8_t)0x3b)
#define REG_TXB0D6_ADDR      ((uint8_t)0x3c)
#define REG_TXB0D7_ADDR      ((uint8_t)0x3d)
#define REG_TXB1CTRL_ADDR    ((uint8_t)0x40)
#define REG_TXB1SIDH_ADDR    ((uint8_t)0x41)
#define REG_TXB1SIDL_ADDR    ((uint8_t)0x42)
#define REG_TXB1EID8_ADDR    ((uint8_t)0x43)
#define REG_TXB1EID0_ADDR    ((uint8_t)0x44)
#define REG_TXB1DLC_ADDR     ((uint8_t)0x45)
#define REG_TXB1D0_ADDR      ((uint8_t)0x46)
#define REG_TXB1D1_ADDR      ((uint8_t)0x47)
#define REG_TXB1D2_ADDR      ((uint8_t)0x48)
#define REG_TXB1D3_ADDR      ((uint8_t)0x49)
#define REG_TXB1D4_ADDR      ((uint8_t)0x4a)
#define REG_TXB1D5_ADDR      ((uint8_t)0x4b)
#define REG_TXB1D6_ADDR      ((uint8_t)0x4c)
#define REG_TXB1D7_ADDR      ((uint8_t)0x4d)
#define REG_TXB2CTRL_ADDR    ((uint8_t)0x50)
#define REG_TXB2SIDH_ADDR    ((uint8_t)0x51)
#define REG_TXB2SIDL_ADDR    ((uint8_t)0x52)
#define REG_TXB2EID8_ADDR    ((uint8_t)0x53)
#define REG_TXB2EID0_ADDR    ((uint8_t)0x54)
#define REG_TXB2DLC_ADDR     ((uint8_t)0x55)
#define REG_TXB2D0_ADDR      ((uint8_t)0x56)
#define REG_TXB2D1_ADDR      ((uint8_t)0x57)
#define REG_TXB2D2_ADDR      ((uint8_t)0x58)
#define REG_TXB2D3_ADDR      ((uint8_t)0x59)
#define REG_TXB2D4_ADDR      ((uint8_t)0x5a)
#define REG_TXB2D5_ADDR      ((uint8_t)0x5b)
#define REG_TXB2D6_ADDR      ((uint8_t)0x5c)
#define REG_TXB2D7_ADDR      ((uint8_t)0x5d)
#define REG_RXB0CTRL_ADDR    ((uint8_t)0x60)
#define REG_RXB0SIDH_ADDR    ((uint8_t)0x61)
#define REG_RXB0SIDL_ADDR    ((uint8_t)0x62)
#define REG_RXB0EID8_ADDR    ((uint8_t)0x63)
#define REG_RXB0EID0_ADDR    ((uint8_t)0x64)
#define REG_RXB0DLC_ADDR     ((uint8_t)0x65)
#define REG_RXB0D0_ADDR      ((uint8_t)0x66)
#define REG_RXB0D1_ADDR      ((uint8_t)0x67)
#define REG_RXB0D2_ADDR      ((uint8_t)0x68)
#define REG_RXB0D3_ADDR      ((uint8_t)0x69)
#define REG_RXB0D4_ADDR      ((uint8_t)0x6a)
#define REG_RXB0D5_ADDR      ((uint8_t)0x6b)
#define REG_RXB0D6_ADDR      ((uint8_t)0x6c)
#define REG_RXB0D7_ADDR      ((uint8_t)0x6d)
#define REG_RXB1CTRL_ADDR    ((uint8_t)0x70)
#define REG_RXB1SIDH_ADDR    ((uint8_t)0x71)
#define REG_RXB1SIDL_ADDR    ((uint8_t)0x72)
#define REG_RXB1EID8_ADDR    ((uint8_t)0x73)
#define REG_RXB1EID0_ADDR    ((uint8_t)0x74)
#define REG_RXB1DLC_ADDR     ((uint8_t)0x75)
#define REG_RXB1D0_ADDR      ((uint8_t)0x76)
#define REG_RXB1D1_ADDR      ((uint8_t)0x77)
#define REG_RXB1D2_ADDR      ((uint8_t)0x78)
#define REG_RXB1D3_ADDR      ((uint8_t)0x79)
#define REG_RXB1D4_ADDR      ((uint8_t)0x7a)
#define REG_RXB1D5_ADDR      ((uint8_t)0x7b)
#define REG_RXB1D6_ADDR      ((uint8_t)0x7c)
#define REG_RXB1D7_ADDR      ((uint8_t)0x7d)

typedef union _BFPCTRL_U_t
{
  struct
  {
    uint8_t B0BFM:1;
    uint8_t B1BFM:1;
    uint8_t B0BFE:1;
    uint8_t B1BFE:1;
    uint8_t B0BFS:1;
    uint8_t B1BFS:1;
    uint8_t :2;
  };
  uint8_t REG_ui8;
} BFPCTRL_U_t;

typedef union _TXRTSCTRL_U_t
{
  struct
  {
    uint8_t B0RTSM:1;
    uint8_t B1RTSM:1;
    uint8_t B2RTSM:1;
    uint8_t B0RTS:1;
    uint8_t B1RTS:1;
    uint8_t B2RTS:1;
    uint8_t :2;
  };
  uint8_t REG_ui8;
} TXRTSCTRL_U_t;

typedef union _CANSTAT_U_t
{
  struct
  {
    uint8_t :1;
    uint8_t ICOD0:1;
    uint8_t ICOD1:1;
    uint8_t ICOD2:1;
    uint8_t :1;
    uint8_t OPMOD0:1;
    uint8_t OPMOD1:1;
    uint8_t OPMOD2:1;
  };
  struct
  {
    uint8_t :1;
    uint8_t ICOD:3;
    uint8_t :1;
    uint8_t OPMOD:3;
  };
  uint8_t REG_ui8;
} CANSTAT_U_t;

typedef union _CANCTRL_U_t
{
  struct
  {
    uint8_t CLKPRE0:1;
    uint8_t CLKPRE1:1;
    uint8_t CLKEN:1;
    uint8_t OSM:1;
    uint8_t ABAT:1;
    uint8_t REQOP0:1;
    uint8_t REQOP1:1;
    uint8_t REQOP2:1;
  };
  struct
  {
    uint8_t CLKPRE:2;
    uint8_t :3;
    uint8_t REQOP:3;
  };
  uint8_t REG_ui8;
} CANCTRL_U_t;

typedef union _CNF3_U_t
{
  struct
  {
    uint8_t PHSEG20:1;
    uint8_t PHSEG21:1;
    uint8_t PHSEG22:1;
    uint8_t :3;
    uint8_t WAKFIL:1;
    uint8_t SOF:1;
  };
  struct
  {
    uint8_t PHSEG2:3;
    uint8_t :5;
  };
  uint8_t REG_ui8;
} CNF3_U_t;

typedef union _CNF2_U_t
{
  struct
  {
    uint8_t PRSEG0:1;
    uint8_t PRSEG1:1;
    uint8_t PRSEG2:1;
    uint8_t PHSEG10:1;
    uint8_t PHSEG11:1;
    uint8_t PHSEG12:1;
    uint8_t SAM:1;
    uint8_t BTLMODE:1;
  };
  struct
  {
    uint8_t PRSEG:3;
    uint8_t PHSEG1:3;
    uint8_t :2;
  };
  uint8_t REG_ui8;
} CNF2_U_t;

typedef union _CNF1_U_t
{
  struct
  {
    uint8_t BRP0:1;
    uint8_t BRP1:1;
    uint8_t BRP2:1;
    uint8_t BRP3:1;
    uint8_t BRP4:1;
    uint8_t BRP5:1;
    uint8_t SJW0:1;
    uint8_t SJW1:1;
  };
  struct
  {
    uint8_t BRP:6;
    uint8_t SJW:2;
  };
  uint8_t REG_ui8;
} CNF1_U_t;

// CANINTE 2B MERRE WAKIE ERRIE TX2IE TX1IE TX0IE RX1IE RX0IE
typedef union _CANINTE_U_t
{
  struct
  {
    uint8_t RX0IE:1;
    uint8_t RX1IE:1;
    uint8_t TX0IE:1;
    uint8_t TX1IE:1;
    uint8_t TX2IE:1;
    uint8_t ERRIE:1;
    uint8_t WAKIE:1;
    uint8_t MERRE:1;
  };
  uint8_t REG_ui8;
} CANINTE_U_t;

// CANINTF 2C MERRF WAKIF ERRIF TX2IF TX1IF TX0IF RX1IF RX0IF
typedef union _CANINTF_U_t
{
  struct
  {
    uint8_t RX0IF:1;
    uint8_t RX1IF:1;
    uint8_t TX0IF:1;
    uint8_t TX1IF:1;
    uint8_t TX2IF:1;
    uint8_t ERRIF:1;
    uint8_t WAKIF:1;
    uint8_t MERRF:1;
  };
  uint8_t REG_ui8;
} CANINTF_U_t;

// EFLG 2D RX1OVR RX0OVR TXBO TXEP RXEP TXWAR RXWAR EWARN
typedef union _EFLG_U_t
{
  struct
  {
    uint8_t EWARN:1;
    uint8_t RXWAR:1;
    uint8_t TXWAR:1;
    uint8_t RXEP:1;
    uint8_t TXEP:1;
    uint8_t TXBO:1;
    uint8_t RX0OVR:1;
    uint8_t RX1OVR:1;
  };
  uint8_t REG_ui8;
} EFLG_U_t;

// TXB0CTRL 30 — ABTF MLOA TXERR TXREQ — TXP1 TXP0
typedef union _TXBnCTRL_U_t
{
  struct
  {
    uint8_t TXP0:1;
    uint8_t TXP1:1;
    uint8_t :1;
    uint8_t TXREQ:1;
    uint8_t TXERR:1;
    uint8_t MLOA:1;
    uint8_t ABTF:1;
    uint8_t :1;
  };
  struct
  {
    uint8_t TXP:2;
    uint8_t :6;
  };
  uint8_t REG_ui8;
} TXBnCTRL_U_t;

// RXB0CTRL 60 — RXM1 RXM0 — RXRTR BUKT BUKT1 FILHIT0 -
typedef union _RXB0CTRL_U_t
{
  struct
  {
    uint8_t FILHIT0:1;
    uint8_t BUKT1:1;
    uint8_t BUKT:1;
    uint8_t RXRTR:1;
    uint8_t :1;
    uint8_t RXM0:1;
    uint8_t RXM1:1;
    uint8_t :1;
  };
  struct
  {
    uint8_t :5;
    uint8_t RXM:2;
    uint8_t :1;
  };
  uint8_t REG_ui8;
} RXB0CTRL_U_t;

// RXB1CTRL
typedef union _RXB1CTRL_U_t
{
  struct
  {
    uint8_t FILHIT0:1;
    uint8_t FILHIT1:1;
    uint8_t FILHIT2:1;
    uint8_t RXRTR:1;
    uint8_t :1;
    uint8_t RXM0:1;
    uint8_t RXM1:1;
    uint8_t :1;
  };
  struct
  {
    uint8_t FILHIT:3;
    uint8_t :2;
    uint8_t RXM:2;
    uint8_t :1;
  };
  uint8_t REG_ui8;
} RXB1CTRL_U_t;

typedef union _SpiStatusReg_mun_t
{
  struct
  {
    uint8_t RX0IF :1;
    uint8_t RX1IF :1;
    uint8_t TXB0REQ :1;
    uint8_t TX0IF :1;
    uint8_t TXB1REQ :1;
    uint8_t TX1IF :1;
    uint8_t TXB2REQ :1;
    uint8_t TX2IF :1;
  };
  uint8_t REG_ui8;
} SpiStatusReg_mun_t;

typedef union _GenericReg_mun_t
{
  RXB1CTRL_U_t  RXB1CTRL;
  RXB0CTRL_U_t  RXB0CTRL;
  TXBnCTRL_U_t  TXBnCTRL;
  EFLG_U_t      EFLG;
  CANINTF_U_t   CANINTF;
  CANINTE_U_t   CANINTE;
  CNF1_U_t      CNF1;
  CNF2_U_t      CNF2;
  CNF3_U_t      CNF3;
  CANCTRL_U_t   CANCTRL;
  CANSTAT_U_t   CANSTAT;
  TXRTSCTRL_U_t TXRTSCTRL;
  BFPCTRL_U_t   BFPCTRL;
  SpiStatusReg_mun_t STATUS;
  uint8_t       REG_ui8;
} GenericReg_mun_t;


typedef enum _DriverStates_mst_t {
  MAIN_IDLE_enm = 0,
  MAIN_READ_BUS_enm,
  MAIN_WRTIE_BUS_enm,
  MAIN_ERROR_enm
} DriverStates_mst_t;

typedef enum _CanIntFlags_menm_t {
  RX0IF_enm = 0,
  RX1IF_enm,
  TX0IF_enm,
  TX1IF_enm,
  TX2IF_enm,
  ERRIF_enm,
  WAKIF_enm,
  MERRF_enm,
  CANINT_NUMBER_OF_FLAGS_enm
} CanIntFlags_menm_t;

typedef struct _MainFlags_mst_t {
  uint32_t device_initialized_flag    :1;
  uint32_t rx_queue_initialized_flag  :1;
  uint32_t tx_queue_initialized_flag  :1;
  uint32_t rxb0_configured_flag       :1;
  uint32_t rxb1_configured_flag       :1;
  uint32_t req_tx_buffer0_flag        :1;
  uint32_t req_tx_buffer1_flag        :1;
  uint32_t req_tx_buffer2_flag        :1;
  uint32_t abort_tx_buffer0_flag      :1;
  uint32_t abort_tx_buffer1_flag      :1;
  uint32_t abort_tx_buffer2_flag      :1;
  uint32_t abort_all_tx_flag          :1;
} MainFlags_mst_t;


/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*-------------------------------[G E N E R A L   T Y P E   D E F I N E S]-----------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/

#define SetReqTxBuffer0Flag_mac()   (main_flags_mst.req_tx_buffer0_flag       = 1)
#define SetReqTxBuffer1Flag_mac()   (main_flags_mst.req_tx_buffer1_flag       = 1)
#define SetReqTxBuffer2Flag_mac()   (main_flags_mst.req_tx_buffer2_flag       = 1)
#define SetDeviceInitFlag_mac()     (main_flags_mst.device_initialized_flag   = 1)
#define SetRXB0InitFlag_mac()       (main_flags_mst.rxb0_configured_flag      = 1)
#define SetRXB1InitFlag_mac()       (main_flags_mst.rxb1_configured_flag      = 1)
#define SetAbortAllTxFlag_mac()     (main_flags_mst.abort_all_tx_flag         = 1)
#define SetTxQueueInitFlag_mac()    (main_flags_mst.tx_queue_initialized_flag = 1)
#define SetRxQueueInitFlag_mac()    (main_flags_mst.rx_queue_initialized_flag = 1)

#define ClearReqTxBuffer0Flag_mac() (main_flags_mst.req_tx_buffer0_flag       = 0)
#define ClearReqTxBuffer1Flag_mac() (main_flags_mst.req_tx_buffer1_flag       = 0)
#define ClearReqTxBuffer2Flag_mac() (main_flags_mst.req_tx_buffer2_flag       = 0)
#define ClearDeviceInitFlag_mac()   (main_flags_mst.device_initialized_flag   = 0)
#define ClearRXB0InitFlag_mac()     (main_flags_mst.rxb0_configured_flag      = 0)
#define ClearRXB1InitFlag_mac()     (main_flags_mst.rxb1_configured_flag      = 0)
#define ClearAbortAllTxFlag_mac()   (main_flags_mst.abort_all_tx_flag         = 0)
#define ClearTxQueueInitFlag_mac()  (main_flags_mst.tx_queue_initialized_flag = 0)
#define ClearRxQueueInitFlag_mac()  (main_flags_mst.rx_queue_initialized_flag = 0)

#define GetReqTxBuffer0Flag_mac()   (main_flags_mst.req_tx_buffer0_flag)
#define GetReqTxBuffer1Flag_mac()   (main_flags_mst.req_tx_buffer1_flag)
#define GetReqTxBuffer2Flag_mac()   (main_flags_mst.req_tx_buffer2_flag)
#define GetDeviceInitFlag_mac()     (main_flags_mst.device_initialized_flag)
#define GetRXB0InitFlag_mac()       (main_flags_mst.rxb0_configured_flag)
#define GetRXB1InitFlag_mac()       (main_flags_mst.rxb1_configured_flag)
#define GetAbortAllTxFlag_mac()     (main_flags_mst.abort_all_tx_flag)
#define GetTxQueueInitFlag_mac()    (main_flags_mst.tx_queue_initialized_flag)
#define GetRxQueueInitFlag_mac()    (main_flags_mst.rx_queue_initialized_flag)

#define SPIWriteBytes_mac(a,b,c,d)      ( device_config_mst.cb_spi_write_bytes_gui8( (a) , (b), (c), (d)) )

#define GetStdIdfromU32_mac(id_u32)     ( ( id_u32 ) & 0x000007FF )
#define GetExdIdfromU32_mac(id_u32)     ( ( ( id_u32 ) & 0x1FFFF800 ) >> 11 )

#define GetTimeStamp_mac(ptr)           ( device_config_mst.cb_time_stamp_gui8(ptr) )

#define ConsumeValue_mac(to , from)     do {(to) = from; (from) = 0;} while (FALSE)

#define Min_mac(a, b)                   (((a) > (b)) ? (b) : (a))

#define Max_mac(a, b)                   (((a) > (b)) ? (a) : (b))

/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*--------------------------------[ V A R I A B L E S   D E F I N I T I O N ]--------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/

// configuration global variables
VISIBILITY DeviceCfg_gst_t               device_config_mst;
VISIBILITY MainFlags_mst_t               main_flags_mst;
VISIBILITY DriverStates_mst_t            main_state_mst;
VISIBILITY TxTransactionID_gu32_t        current_trans_id_mu32;

// queues global variables
VISIBILITY RxCanContainer_gst_t          RxQueueTable[RX_QUEUE_SIZE];
VISIBILITY TxCanContainer_gst_t          TxQueueTable[TX_QUEUE_SIZE];
VISIBILITY QueueToken_gst_t              rx_queue_token_mst;
VISIBILITY QueueToken_gst_t              tx_queue_token_mst;


/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*---------------------------------[F U N C T I O N S   P R O T O T Y P E S]---------------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/

VISIBILITY void copy_filter_value_mvd(Boolean is_extended_fbl, uint32_t filter_value_fu32, uint8_t *to_array_fpui8);

VISIBILITY void copy_mask_value_mvd(Boolean is_extd_fbl, uint32_t from_mask_value_fui32, uint8_t *to_array_fpui8);

VISIBILITY void set_tx_buffer_abort_flag_mvd ( TxBufferNbr_gui8_t buffer_nbr_fui8 );

VISIBILITY void clear_tx_buffer_abort_flag_mvd ( TxBufferNbr_gui8_t buffer_nbr_fui8 );

VISIBILITY StdReturn_gui8_t abort_tx_buffer_mui8 ( TxBufferNbr_gui8_t buffer_nbr_fui8 );

VISIBILITY StdReturn_gui8_t fill_tx_buffer_mui8 ( const CanMsg_gst_t * tx_msg_fpst, uint8_t *buffer_to_fpui8 );

VISIBILITY StdReturn_gui8_t read_status_reg_mui8 ( uint8_t * status_reg_fpui8 );

VISIBILITY void get_valid_time_stamp_mvd ( TimeStamp_gst_t * time_stamp_fst );

VISIBILITY StdReturn_gui8_t clear_TXBnREQ_mui8 ( TxBufferNbr_gui8_t tx_buffer_nbr_lui8 );

VISIBILITY void copy_rx_raw_buffer_mvd ( const uint8_t * rx_raw_buffer_fpui8, CanMsg_gst_t *msg_fpst );

VISIBILITY StdReturn_gui8_t read_rx_buffer_mui8 ( RxBufferNbr_gui8_t buffer_nbr_fui8, CanMsg_gst_t *msg_fpst );

VISIBILITY StdReturn_gui8_t transmit_msg_mui8 ( const CanMsg_gst_t * tx_msg_fpst, TxBufferNbr_gui8_t tx_buffer_nbr_lui8 );

VISIBILITY StdReturn_gui8_t clear_CANINT_flag_mui8 ( CanIntFlags_menm_t flag_fenm );

VISIBILITY TxTransactionID_gu32_t get_current_trans_id_mu32 ( void );

VISIBILITY void convert_to_mcp_order_mvd ( Boolean is_extd_fpbl, uint32_t from_val_fu32, uint8_t * to_buffer_fpui8 );

StdReturn_gui8_t device_init_gui8 ( const DeviceCfg_gst_t * device_cfg_fpst );

void mcp2515_main_gvd ( void );

StdReturn_gui8_t abort_all_tx_buffers_gui8 ( void );

StdReturn_gui8_t abort_tx_buffers_gui8 ( TxBufferNbr_gui8_t buffers_fui8 );

StdReturn_gui8_t set_rx0_buffer_settings_gui8(const RXB0_ConfigSet_gst_t * rxb0_cfg_fpst);

StdReturn_gui8_t set_rx1_buffer_settings_gui8( const RXB1_ConfigSet_gst_t * rxb1_cfg_fpst);

StdReturn_gui8_t set_operation_mode_gui8 ( OperationMode_genm_t mode_of_op_fenm );

StdReturn_gui8_t write_can_msg_gui8 ( TxCanContainer_gst_t * tx_can_msg_fpst, uint32_t * queue_usage_fpu32);

StdReturn_gui8_t read_can_msg_gui8(RxCanContainer_gst_t * rx_can_msg_fpst, uint32_t * queue_usage_fpu32);

StdReturn_gui8_t mcp2515_deinit_gui8 ( void );





/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*------------------------[P U B L I C   F U N C T I O N S   D E F I N I T I O N]----------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/

/*
 * device_init_gui8 function: full description in the header file
 */
StdReturn_gui8_t device_init_gui8( const DeviceCfg_gst_t * device_cfg_fpst )
{
  StdReturn_gui8_t last_return_lui8 = OP_NOK;
  GenericReg_mun_t reg_config_lun;
  uint8_t spi_trans_lpui8[ 4 ];

  //copy configuration to internal device configuration
  if ( device_cfg_fpst != NULL_PTR ) {
    reg_config_lun.REG_ui8 = 0;
    current_trans_id_mu32 = 0;
    memset( &main_flags_mst , 0x00 , sizeof( main_flags_mst ) );
    memcpy( &device_config_mst , device_cfg_fpst , sizeof(DeviceCfg_gst_t) );
    memset( &spi_trans_lpui8[ 0 ] , 0x00 , sizeof( spi_trans_lpui8 ) );
    if ( device_config_mst.cb_spi_write_bytes_gui8 != NULL_PTR ) {
      // reset the module
      last_return_lui8 = SPIWriteBytes_mac( SPI_RESET_INST , SPI_NO_ADDR , NULL_PTR , 0 );
      if ( last_return_lui8 == OP_OK ) {
        // check if the module is in configuration mode
        last_return_lui8 = SPIWriteBytes_mac( SPI_READ_INST , REG_CANSTAT_ADDR , &reg_config_lun.REG_ui8 , 1 );
        if ( ( last_return_lui8 == OP_OK ) && ( reg_config_lun.CANSTAT.OPMOD == CONFIG_MODE_enm ) ) {
          reg_config_lun.REG_ui8 = 0;
          reg_config_lun.CNF3.PHSEG2 = PHASE_SEGMENT_2;
          ConsumeValue_mac( spi_trans_lpui8[ 0 ] , reg_config_lun.REG_ui8 );
          reg_config_lun.CNF2.PHSEG1 = PHASE_SEGMENT_1;
          reg_config_lun.CNF2.PRSEG = PROPAGATION_SEGMENT;
          reg_config_lun.CNF2.SAM = SAMPLE_POINT_CONFIG;
          reg_config_lun.CNF2.BTLMODE = 1;  // set the phase segment 2 by CNF3
          ConsumeValue_mac( spi_trans_lpui8[ 1 ] , reg_config_lun.REG_ui8 );
          reg_config_lun.CNF1.BRP = REG_BRP_VALUE;
          reg_config_lun.CNF1.SJW = SYNC_JUMP_WIDTH;
          ConsumeValue_mac( spi_trans_lpui8[ 2 ] , reg_config_lun.REG_ui8 );
          last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_CNF3_ADDR , &spi_trans_lpui8[ 0 ] , 3 );
          if ( ( last_return_lui8 == OP_OK ) && ( device_config_mst.rx_buffer_0_cfg_pst != NULL_PTR ) ) {
            //set the filters and mask from de_cfg structure and change to the desired mode
            last_return_lui8 = set_rx0_buffer_settings_gui8( device_config_mst.rx_buffer_0_cfg_pst );
            if ( last_return_lui8 != OP_OK ) {
              // Error while setting the RX_BUFFER_0
              last_return_lui8 = ERROR_RXB0_SET_OP;
            }
          }
          if ( ( last_return_lui8 == OP_OK ) && ( device_config_mst.rx_buffer_1_cfg_pst != NULL_PTR ) ) {
            last_return_lui8 = set_rx1_buffer_settings_gui8( device_config_mst.rx_buffer_1_cfg_pst );
            if ( last_return_lui8 != OP_OK ) {
              // Error while setting the RXB1 mask and filters
              last_return_lui8 = ERROR_RXB1_SET_OP;
            }
          }
          // Disable all the interrupts
          if ( last_return_lui8 == OP_OK ) {
            // TODO add support to activate interrupts in the future
            // Set the CANINTE register
            reg_config_lun.CANINTE.REG_ui8 = 0;
            ConsumeValue_mac( spi_trans_lpui8[ 0 ] , reg_config_lun.REG_ui8 );
            // Set the CANINTF register
            reg_config_lun.CANINTF.REG_ui8 = 0;
            ConsumeValue_mac( spi_trans_lpui8[ 1 ] , reg_config_lun.REG_ui8 );
            // Set the EFLG register
            reg_config_lun.EFLG.RX0OVR = 0;
            reg_config_lun.EFLG.RX1OVR = 0;
            ConsumeValue_mac( spi_trans_lpui8[ 2 ] , reg_config_lun.REG_ui8 );
            // Write SPI, Base address CANINTE
            last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_CANINTE_ADDR , &spi_trans_lpui8[ 0 ] , 3 );
          }
          // Disable RXnBF pins
          if ( last_return_lui8 == OP_OK ) {
            reg_config_lun.BFPCTRL.REG_ui8 = 0;
            ConsumeValue_mac( spi_trans_lpui8[ 0 ] , reg_config_lun.REG_ui8 );
            last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_BFPCTRL_ADDR , &spi_trans_lpui8[ 0 ] , 1 );
          }
          // Disable TXnRTS pins
          if ( last_return_lui8 == OP_OK ) {
            reg_config_lun.TXRTSCTRL.B0RTSM = 0;
            reg_config_lun.TXRTSCTRL.B1RTSM = 0;
            reg_config_lun.TXRTSCTRL.B2RTSM = 0;
            spi_trans_lpui8[ 0 ] = 0x07; // Mask 0000 0111
            ConsumeValue_mac( spi_trans_lpui8[ 1 ] , reg_config_lun.REG_ui8 );
            last_return_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_TXRTSCTRL_ADDR , &spi_trans_lpui8[ 0 ] , 2 );
          }
          // Initialize Tx Queue
          if ( last_return_lui8 == OP_OK ) {
            last_return_lui8 = create_queue_gui8( TX_QUEUE_SIZE , sizeof(TxCanContainer_gst_t) , &TxQueueTable[ 0 ] ,
                &tx_queue_token_mst );
            if ( last_return_lui8 == OP_OK ) {
              SetTxQueueInitFlag_mac();
            } else {
              // just to make sure is not set
              ClearTxQueueInitFlag_mac();
            }
          }
          // Initialize Rx Queue
          if ( last_return_lui8 == OP_OK ) {
            last_return_lui8 = create_queue_gui8( RX_QUEUE_SIZE , sizeof(RxCanContainer_gst_t) , &RxQueueTable[ 0 ] ,
                &rx_queue_token_mst );
            if ( last_return_lui8 == OP_OK ) {
              SetRxQueueInitFlag_mac();
            } else {
              // just to make sure is not set
              ClearRxQueueInitFlag_mac();
            }
          }
          // Set operation mode
          if ( last_return_lui8 == OP_OK ) {
            reg_config_lun.CANCTRL.REG_ui8 = 0;
            if ( ( device_config_mst.operation_mode_enm >= 0 ) && ( device_config_mst.operation_mode_enm < NUMBER_OF_OP_MODES_enm ) ) {
              reg_config_lun.CANCTRL.REQOP = (uint8_t) device_config_mst.operation_mode_enm;
            } else {
              reg_config_lun.CANCTRL.REQOP = (uint8_t) NORMAL_MODE_enm;
            }
            // Set tx mode
            if ( ( device_config_mst.tx_mode_enm >= 0 ) && ( device_config_mst.tx_mode_enm < NUMBER_OF_TX_MODE_enm ) ) {
              reg_config_lun.CANCTRL.OSM = (uint8_t) device_config_mst.tx_mode_enm;
            } else {
              reg_config_lun.CANCTRL.OSM = (uint8_t) TX_NORMAL_MODE_enm;
            }
            reg_config_lun.CANCTRL.CLKEN = 0; // CLKOUT pin is disabled
            spi_trans_lpui8[ 0 ] = 0xE8; // Mask value 1110 1000
            ConsumeValue_mac( spi_trans_lpui8[ 1 ] , reg_config_lun.REG_ui8 );
            last_return_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_CANCTRL_ADDR , &spi_trans_lpui8[ 0 ] , 2 );
            if ( last_return_lui8 == OP_OK ) {
              // Device initialized correctly
              SetDeviceInitFlag_mac();
            } else {
              // just to make sure that the flag is not set
              ClearDeviceInitFlag_mac();
            }
          }
        } else {
          // MCU not in configuration mode
          last_return_lui8 = ERROR_OPMODE_SET_OP;
        }
      } else {
        //MCP2515 reset operation failed
        last_return_lui8 = ERROR_RESET_REQ_OP;
      }
    } else {
      //SPI read callback pointer is null
      last_return_lui8 = ERROR_NULL_PTR;
    }
  } else {
    //device configuration structure pointer is null
  }
  return last_return_lui8;
} // end of device_init_gui8()

/*
 * set_rx0_buffer_settings_gui8 function: full description in the header file
 */
StdReturn_gui8_t set_rx0_buffer_settings_gui8( const RXB0_ConfigSet_gst_t * rxb0_cfg_fpst )
{
  StdReturn_gui8_t last_return_lui8 = OP_NOK;
  uint8_t spi_trans_lpui8[ 8 ];
  GenericReg_mun_t reg_config_lun;

  if ( rxb0_cfg_fpst != NULL_PTR ) {
    // masks and filters should be configured only in Configuration mode
    last_return_lui8 = set_operation_mode_gui8( CONFIG_MODE_enm );
    if ( last_return_lui8 == OP_OK ) {
      memset( spi_trans_lpui8 , 0 , sizeof( spi_trans_lpui8 ) );
      copy_filter_value_mvd( rxb0_cfg_fpst->is_extended_bl , rxb0_cfg_fpst->rxb0_accept_data_st.filters_pst[ 0 ] ,
          &spi_trans_lpui8[ 0 ] );
      copy_filter_value_mvd( rxb0_cfg_fpst->is_extended_bl , rxb0_cfg_fpst->rxb0_accept_data_st.filters_pst[ 1 ] ,
          &spi_trans_lpui8[ 4 ] );
      // REG_RXF0SIDH_ADDR as base add 00h, continuous 8 bytes write
      last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_RXF0SIDH_ADDR , &spi_trans_lpui8[ 0 ] , 8 );
      if ( last_return_lui8 == OP_OK ) {
        // filters are configured correctly, configure the mask value
        memset( spi_trans_lpui8 , 0 , sizeof( spi_trans_lpui8 ) );
        copy_mask_value_mvd( rxb0_cfg_fpst->is_extended_bl , rxb0_cfg_fpst->rxb0_accept_data_st.rx_mask_u32 , &spi_trans_lpui8[ 0 ] );
        // write value to registers
        last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_RXM0SIDH_ADDR , &spi_trans_lpui8[ 0 ] , 4 );
        if ( last_return_lui8 == OP_OK ) {
          reg_config_lun.REG_ui8 = 0;
          // Set the RXB0CTRL
          if ( rxb0_cfg_fpst->filters_masks_enable_bl == TRUE ) {
            // Enable filters
            reg_config_lun.RXB0CTRL.RXM = 0;
          } else {
            // Disable filters
            reg_config_lun.RXB0CTRL.RXM = 3;
          }
          if ( rxb0_cfg_fpst->roll_over_enable_bl == TRUE ) {
            reg_config_lun.RXB0CTRL.BUKT = 1;
          } else {
            reg_config_lun.RXB0CTRL.BUKT = 0;
          }
          spi_trans_lpui8[ 0 ] = 0x60; // Mask value for RXB1CTRL
          spi_trans_lpui8[ 1 ] = reg_config_lun.REG_ui8;
          last_return_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_RXB0CTRL_ADDR , &spi_trans_lpui8[ 0 ] , 2 );
          if ( ( last_return_lui8 == OP_OK ) && ( GetRXB0InitFlag_mac() == 0 ) ) {
            // set the RXB0 init flag
            SetRXB0InitFlag_mac();
          }
        } else {
          // Setting Mask 0 error
        }
      } else {
        // SPI write error
        last_return_lui8 = ERROR_SPI_WRITE_OP;
      }
    } else {
      // Error switch mode
      last_return_lui8 = ERROR_SET_MODE_OP;
    }
  }
  return last_return_lui8;
} //End of set_rx0_buffer_settings_gui8()

/*
 * set_rx1_buffer_settings_gui8 function: full description in the header file
 */
StdReturn_gui8_t set_rx1_buffer_settings_gui8( const RXB1_ConfigSet_gst_t * rxb1_cfg_fpst )
{
  StdReturn_gui8_t last_return_lui8 = OP_NOK;
  uint8_t loop_counter_lui8;
  uint8_t spi_trans_lpui8[ 12 ];
  GenericReg_mun_t reg_config_lun;

  if ( rxb1_cfg_fpst != NULL_PTR ) {
    last_return_lui8 = set_operation_mode_gui8( CONFIG_MODE_enm );
    if ( last_return_lui8 == OP_OK ) {
      memset( spi_trans_lpui8 , 0 , sizeof( spi_trans_lpui8 ) );
      for ( loop_counter_lui8 = 0; loop_counter_lui8 < 3; loop_counter_lui8++ ) {
        copy_filter_value_mvd( rxb1_cfg_fpst->is_extended_bl , rxb1_cfg_fpst->rxb1_accept_data_st.filters_pst[ loop_counter_lui8 ] ,
            &spi_trans_lpui8[ ( loop_counter_lui8 * 4 ) ] );
      }
      // RXF3SIDH as base address 10h, continuous 12 bytes.
      last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_RXF3SIDH_ADDR , &spi_trans_lpui8[ 0 ] , 12 );
      if ( last_return_lui8 == OP_OK ) {
        // rest the 1st 4bytes
        memset( spi_trans_lpui8 , 0 , 4 );
        // configure the latest filter for RX buffer 1
        copy_filter_value_mvd( rxb1_cfg_fpst->is_extended_bl , rxb1_cfg_fpst->rxb1_accept_data_st.filters_pst[ 3 ] ,
            &spi_trans_lpui8[ 0 ] );
        // RXF2SIDH as base address 08h, continuous 4 bytes
        last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_RXF2SIDH_ADDR , &spi_trans_lpui8[ 0 ] , 4 );
        if ( last_return_lui8 == OP_OK ) {
          // rest the 1st 4bytes
          memset( spi_trans_lpui8 , 0 , 4 );
          // filters are configured correctly, configure the mask value
          copy_mask_value_mvd( rxb1_cfg_fpst->is_extended_bl , rxb1_cfg_fpst->rxb1_accept_data_st.rx_mask_u32 , &spi_trans_lpui8[ 0 ] );
          // write value to register 24h, continuous 4  bytes.
          last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , REG_RXM1SIDH_ADDR , &spi_trans_lpui8[ 0 ] , 4 );
          if ( last_return_lui8 == OP_OK ) {
            reg_config_lun.REG_ui8 = 0;
            // Set the RXB0CTRL
            if ( rxb1_cfg_fpst->filters_masks_enable_bl == TRUE ) {
              // Enable filters
              reg_config_lun.RXB0CTRL.RXM = 0;
            } else {
              // Disable filters
              reg_config_lun.RXB0CTRL.RXM = 3;
            }
            spi_trans_lpui8[ 0 ] = 0x60; // Mask value for RXB1CTRL
            spi_trans_lpui8[ 1 ] = reg_config_lun.REG_ui8;
            last_return_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_RXB1CTRL_ADDR , &spi_trans_lpui8[ 0 ] , 2 );
            if ( ( last_return_lui8 == OP_OK ) && ( GetRXB1InitFlag_mac() == 0 ) ) {
              SetRXB1InitFlag_mac();
            }
          } else {
            // Error setting MASK 1
          }
        } else {
          // SPI write error
          last_return_lui8 = ERROR_SPI_WRITE_OP;
        }
      } else {
        // SPI write error
        last_return_lui8 = ERROR_SPI_WRITE_OP;
      }
    } else {
      // Mode switch error
      last_return_lui8 = ERROR_SET_MODE_OP;
    }
  }
  return last_return_lui8;
} // End of set_rx1_buffer_settings_gui8()

/*
 * Full description in the header file
 */
StdReturn_gui8_t set_operation_mode_gui8( OperationMode_genm_t mode_of_op_fenm )
{
  StdReturn_gui8_t last_ret_lui8 = OP_NOK;
  uint8_t spi_trans_lpui8[ 2 ];
  GenericReg_mun_t generic_reg_lun;

  generic_reg_lun.REG_ui8 = 0;
  if ( ( mode_of_op_fenm >= 0 ) && ( mode_of_op_fenm < NUMBER_OF_OP_MODES_enm ) ) {
    generic_reg_lun.CANCTRL.REQOP = (uint8_t) mode_of_op_fenm;
    spi_trans_lpui8[ 0 ] = 0xE0; /**< Mask value 1110 0000 */
    ConsumeValue_mac( spi_trans_lpui8[ 1 ] , generic_reg_lun.REG_ui8 );
    last_ret_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_CANCTRL_ADDR , &spi_trans_lpui8[ 0 ] , 2 );
    if ( last_ret_lui8 == OP_OK ) {
      // SPI write done, read operation state
      last_ret_lui8 = SPIWriteBytes_mac( SPI_READ_INST , REG_CANSTAT_ADDR , &generic_reg_lun.REG_ui8 , 1 );
      if ( last_ret_lui8 == OP_OK ) {
        // SPI read done, check if the mode is set
        // aux_array_lpui8[1] holds old value, aux_array_lpui8[0] holds the new read value
        if ( generic_reg_lun.CANSTAT.OPMOD != mode_of_op_fenm ) {
          // mode was set correctly
          last_ret_lui8 = OP_NOK;
        }
      } else {
        // SPI read fail
        last_ret_lui8 = ERROR_SPI_READ_OP;
      }
    } else {
      // SPI write fail
      last_ret_lui8 = ERROR_SPI_WRITE_OP;
    }
  } else {
    // Incorrect mode
    last_ret_lui8 = ERROR_SET_MODE_OP;
  }
  return last_ret_lui8;
}

/*
 *  main_function_gvd function: full description in header file
 */
void mcp2515_main_gvd( void )
{
  StdReturn_gui8_t op_result_lui8 = OP_NOK;
  TxCanContainer_gst_t read_write_buffer_lst;
  SpiStatusReg_mun_t status_reg_lun;
  QueueState_genm_t rx_queue_state_lenm = QUEUE_INIT_enm;
  QueueState_genm_t tx_queue_state_lenm = QUEUE_INIT_enm;
  static Boolean is_first_run_lbl = TRUE;

  if ( is_first_run_lbl == TRUE ) {
    status_reg_lun.REG_ui8 = 0;
    memset( (uint8_t*) &read_write_buffer_lst , 0x00 , sizeof(TxCanContainer_gst_t) );
    is_first_run_lbl = FALSE;
  }
  ATOMIC_BEGIN_MAC( );
  (void) get_queue_state_gui8( &rx_queue_token_mst , &rx_queue_state_lenm );
  (void) get_queue_state_gui8( &tx_queue_token_mst , &tx_queue_state_lenm );
  ATOMIC_END_MAC( );
  // SPI read: status flags
  op_result_lui8 = read_status_reg_mui8( &status_reg_lun.REG_ui8 );
  if ( op_result_lui8 == OP_OK ) {
    switch ( main_state_mst ) {
      case MAIN_IDLE_enm: {
        if ( ( GetDeviceInitFlag_mac() == 0 ) || ( rx_queue_state_lenm == QUEUE_INIT_enm )
            || ( tx_queue_state_lenm == QUEUE_INIT_enm ) ) {
          // device not initialized or RX and Tx are not created yet
          main_state_mst = MAIN_ERROR_enm;
        } else {
          if ( ( ( status_reg_lun.RX0IF == 1 ) || ( status_reg_lun.RX1IF == 1 ) ) && ( rx_queue_state_lenm != QUEUE_FULL_enm ) ) {
            // move Read BUS state
            main_state_mst = MAIN_READ_BUS_enm;
          } else {
            // No valid message in the Rx buffers
            if ( tx_queue_state_lenm != QUEUE_EMPTY_enm ) {
              main_state_mst = MAIN_WRTIE_BUS_enm;
            }
          }
        }
        break;
      }
      case MAIN_READ_BUS_enm: {
        if ( status_reg_lun.RX0IF == 1 ) {
          if ( rx_queue_state_lenm != QUEUE_FULL_enm ) {
            get_valid_time_stamp_mvd( &read_write_buffer_lst.can_container_st.time_stamp_st );
            op_result_lui8 = read_rx_buffer_mui8( RX_BUFFER_0 , &read_write_buffer_lst.can_container_st.can_msg_data_st );
            if ( op_result_lui8 == OP_OK ) {
              // push new message to rx_queue
              ATOMIC_BEGIN_MAC( );
              op_result_lui8 = queue_push_gui8( &rx_queue_token_mst , &read_write_buffer_lst.can_container_st );
              ATOMIC_END_MAC( );
              if ( op_result_lui8 != OP_OK ) {
                // TODO Trace the error value
              }
            } else {
              // ERROR while reading the rx buffer 0
              //TODO trace the error value
            }
          } else {
            // TODO trace rx_queue full
            main_state_mst = MAIN_IDLE_enm;
          }
        }
        if ( status_reg_lun.RX1IF == 1 ) {
          if ( rx_queue_state_lenm != QUEUE_FULL_enm ) {
            get_valid_time_stamp_mvd( &read_write_buffer_lst.can_container_st.time_stamp_st );
            op_result_lui8 = read_rx_buffer_mui8( RX_BUFFER_1 , &read_write_buffer_lst.can_container_st.can_msg_data_st );
            if ( op_result_lui8 == OP_OK ) {
              // push new message to rx_queue
              ATOMIC_BEGIN_MAC( );
              op_result_lui8 = queue_push_gui8( &rx_queue_token_mst , &read_write_buffer_lst.can_container_st );
              ATOMIC_END_MAC( );
              if ( op_result_lui8 != OP_OK ) {
                // TODO Trace the error value
              }
            } else {
              // ERROR while reading the rx buffer 0
              //TODO trace the error value
            }
          } else {
            // TODO trace rx_queue full
            main_state_mst = MAIN_IDLE_enm;
          }
        }
        if ( ( status_reg_lun.RX1IF != 1 ) && ( status_reg_lun.RX0IF != 1 ) ) {
          // No message received
          main_state_mst = MAIN_IDLE_enm;
        }
        break;
      }
      case MAIN_WRTIE_BUS_enm: {
        if ( tx_queue_state_lenm == QUEUE_EMPTY_enm ) {
          main_state_mst = MAIN_IDLE_enm;
        } else {
          if ( status_reg_lun.TXB0REQ == 0 ) {
            // clear TX0IF before transmitting a new message
            if ( status_reg_lun.TX0IF ) {
              op_result_lui8 = clear_CANINT_flag_mui8( TX0IF_enm );
            }
            if ( op_result_lui8 == OP_OK ) {
              // TXB0 is transmitted correctly, transmit a new element from the queue
              // Tx_Queue is not empty, put the message in TXB0 and trigger the transmission
              ATOMIC_BEGIN_MAC( );
              op_result_lui8 = queue_pop_gui8( &tx_queue_token_mst , &read_write_buffer_lst );
              ATOMIC_END_MAC( );
              if ( op_result_lui8 == OP_OK ) {
                op_result_lui8 = transmit_msg_mui8( &read_write_buffer_lst.can_container_st.can_msg_data_st , TX_BUFFER_0 );
                if ( op_result_lui8 == OP_NOK ) {
                  //TODO think about trancing the error, SPI write error
                }
              } else {
                // TX queue is not empty already checked at the beginning
              }
            } else {
              // Error while clearing the TX0IF
            }
          }
          if ( status_reg_lun.TXB1REQ == 0 ) {
            // clear TX1IF before transmitting a new message
            if ( status_reg_lun.TX1IF ) {
              op_result_lui8 = clear_CANINT_flag_mui8( TX1IF_enm );
            }
            if ( op_result_lui8 == OP_OK ) {
              // TXB1 last transmission done, transmit new element from the queue
              // Tx_Queue is not empty, put the message in TXB1 and trigger the transmission
              ATOMIC_BEGIN_MAC( );
              op_result_lui8 = queue_pop_gui8( &tx_queue_token_mst , &read_write_buffer_lst );
              ATOMIC_END_MAC( );
              if ( op_result_lui8 == OP_OK ) {
                op_result_lui8 = transmit_msg_mui8( &read_write_buffer_lst.can_container_st.can_msg_data_st , TX_BUFFER_1 );
                if ( op_result_lui8 == OP_NOK ) {
                  //TODO think about trancing the error
                }
              } else {
                // Queue empty move to the MAIN_IDLE state
                main_state_mst = MAIN_IDLE_enm;
              }
            } else {
              //Error while clearing TX1IF
            }
          }
          if ( status_reg_lun.TXB2REQ == 0 ) {
            // clear TX2IF before transmitting a new message
            if ( status_reg_lun.TX2IF ) {
              op_result_lui8 = clear_CANINT_flag_mui8( TX2IF_enm );
            }
            if ( op_result_lui8 == OP_OK ) {
              // TXB2 last transmission done, transmit new element from the queue
              // Tx_Queue is not empty, put the message in TXB2 and trigger the transmission
              ATOMIC_BEGIN_MAC( );
              op_result_lui8 = queue_pop_gui8( &tx_queue_token_mst , &read_write_buffer_lst );
              ATOMIC_END_MAC( );
              if ( op_result_lui8 == OP_OK ) {
                op_result_lui8 = transmit_msg_mui8( &read_write_buffer_lst.can_container_st.can_msg_data_st , TX_BUFFER_2 );
                if ( op_result_lui8 == OP_NOK ) {
                  //TODO think about trancing the error
                }
              } else {
                // Queue empty, move to the MAIN_IDLE state
                main_state_mst = MAIN_IDLE_enm;
              }
            } else {
              // Error while clearing TX2IF
            }
          }
          // Jump to IDLE state after transmitting at most 3 messages.
          main_state_mst = MAIN_IDLE_enm;
        }
        break;
      }
      case MAIN_ERROR_enm: {
        // TODO think about the healing criteria
        break;
      }
      default:
        break;
        // this part should reached
    }
  } else {
    // SPI read error
    //TODO think about tracing the errors
  }
  return;
} // End of main_function_gvd

/*
 * abort_tx_buffers_gui8 function: Full description in the header file
 */
StdReturn_gui8_t abort_tx_buffers_gui8( TxBufferNbr_gui8_t buffers_fui8 )
{
  StdReturn_gui8_t ret_val_lui8 = OP_OK;
  uint8_t count_lui8 = 0;

  //normalize buffers number possible combination 0000 0111
  buffers_fui8 &= 0x07;
  do {
    if ( ( buffers_fui8 & 0x01 ) == 0x01 ) {
      ret_val_lui8 |= abort_tx_buffer_mui8( count_lui8 );
    }
    count_lui8++;
  } while ( buffers_fui8 != 0 );

  return ret_val_lui8;
}

/*
 * abort_all_tx_buffers_gui8 function: full description in the header file
 */
StdReturn_gui8_t abort_all_tx_buffers_gui8( void )
{
  StdReturn_gui8_t ret_val_lui8 = OP_NOK;
  uint8_t spi_trans_pui8[ 2 ];

  spi_trans_pui8[ 0 ] = 0x10; // Mask value
  spi_trans_pui8[ 1 ] = 0x10; // Bit value
  ret_val_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_CANCTRL_ADDR , &spi_trans_pui8[ 0 ] , 2 );
  if ( ret_val_lui8 == OP_OK ) {
    SetAbortAllTxFlag_mac();
  }
  return ret_val_lui8;
}

StdReturn_gui8_t write_can_msg_gui8( TxCanContainer_gst_t * tx_can_msg_fpst, uint32_t * queue_usage_fpu32 )
{
  StdReturn_gui8_t op_result_lui8 = OP_NOK;

  if ( tx_can_msg_fpst != NULL_PTR ) {
    // Access the queue qhould be Atomic
    ATOMIC_BEGIN_MAC( );
    op_result_lui8 = queue_push_gui8( &tx_queue_token_mst , tx_can_msg_fpst );
    if ( op_result_lui8 == OP_OK ) {
      tx_can_msg_fpst->trans_id_u32 = get_current_trans_id_mu32( );
      op_result_lui8 = get_queue_usage_gui8( &tx_queue_token_mst , queue_usage_fpu32 );
    } else {
      // Queue push error
      op_result_lui8 = ERROR_QUEUE_PUSH_OP;
    }
    ATOMIC_END_MAC( );
  }
  return op_result_lui8;
}

StdReturn_gui8_t read_can_msg_gui8( RxCanContainer_gst_t * rx_can_msg_fpst, uint32_t * queue_usage_fpu32 )
{
  StdReturn_gui8_t op_result_lui8 = OP_NOK;
  if ( rx_can_msg_fpst != NULL_PTR ) {
    // Access the queue qhould be Atomic
    ATOMIC_BEGIN_MAC( );
    op_result_lui8 = queue_pop_gui8( &rx_queue_token_mst , rx_can_msg_fpst );
    if ( op_result_lui8 == OP_OK ) {
      op_result_lui8 = get_queue_usage_gui8( &rx_queue_token_mst , queue_usage_fpu32 );
    }
    ATOMIC_END_MAC( );
  }
  return op_result_lui8;
}


StdReturn_gui8_t mcp2515_deinit_gui8 ( void )
{
  StdReturn_gui8_t result_lui8 = OP_NOK;
  // reset the module
  result_lui8 = SPIWriteBytes_mac( SPI_RESET_INST , SPI_NO_ADDR , NULL_PTR , 0 );
  if ( result_lui8 == OP_OK ) {
    ATOMIC_BEGIN_MAC();
    result_lui8 = delete_queue_gui8( &tx_queue_token_mst );
    ATOMIC_END_MAC();
    if ( result_lui8 == OP_OK ) {
      ATOMIC_BEGIN_MAC();
      result_lui8 = delete_queue_gui8( &rx_queue_token_mst );
      ATOMIC_END_MAC();
      if ( result_lui8 == OP_OK ) {
        // reset all the flags
          ClearDeviceInitFlag_mac();
          ClearRXB0InitFlag_mac();
          ClearRXB1InitFlag_mac();
          ClearRxQueueInitFlag_mac();
          ClearTxQueueInitFlag_mac();
      }
    }
  } else {
    // reset module error
  }
  return result_lui8;
}
/***********************************************************************************************************\
*-----------------------------------------------------------------------------------------------------------*
*-----------------------[S T A T I C   F U N C T I O N S   D E F I N I T I O N]-----------------------------*
*-----------------------------------------------------------------------------------------------------------*
\***********************************************************************************************************/
/**
 * \brief function gives a unique Tx transaction ID
 *
 * \return unique id value
 *
 * \param void
 *
 */
VISIBILITY TxTransactionID_gu32_t get_current_trans_id_mu32( void )
{
  if ( ++current_trans_id_mu32 == UINT32_MAX ) {
    current_trans_id_mu32 = 0;
  }
  return current_trans_id_mu32;
}
/**
 * \brief Reads the status flags, useful to check the state of Rx an Tx buffers.
 *
 * \return OP_OK if the value read correctly, OP_NOK if any SPI read issue.
 *
 * \param status_reg_fpui8 as output, holds the status flags.
 *
 */
VISIBILITY StdReturn_gui8_t read_status_reg_mui8( uint8_t * status_reg_fpui8 )
{
  StdReturn_gui8_t ret_value_lui8 = OP_NOK;

  if ( status_reg_fpui8 != NULL_PTR ) {
    ret_value_lui8 = SPIWriteBytes_mac( SPI_READ_STATUS_INST , SPI_NO_ADDR , status_reg_fpui8 , 1 );
  } else {
    // Null pointer as parameter
    ret_value_lui8 = ERROR_NULL_PTR;
  }
  return ret_value_lui8;
}

/**
 *
 * \brief Converts a standard unsigned 32 value to MCP2515 format registers format, valid for masks and filters.
 *
 * \return void.
 *
 * \param is_extd_fpbl as input indicate if the mask or the filter is extended.
 *
 * \param from_val_u32 as input holds the filter or the mask value.
 *
 * \param to_buffer_fpui8 as output the buffer that will be written to the MCP2515.
 *
 * \note This function assumes that the output buffer is initialized to 0.
 */
VISIBILITY void convert_to_mcp_order_mvd( Boolean is_extd_fpbl, uint32_t from_val_fu32, uint8_t * to_buffer_fpui8 )
{
  // from 0 - 7
  to_buffer_fpui8[ 0 ] = (uint8_t) ( GetStdIdfromU32_mac(from_val_fu32) >> 3 );
  // from 8 - 15
  to_buffer_fpui8[ 1 ] = (uint8_t) ( ( GetStdIdfromU32_mac(from_val_fu32) & 0x07 ) << 5 );
  if ( is_extd_fpbl == TRUE ) {
    to_buffer_fpui8[ 1 ] |= (uint8_t) ( GetExdIdfromU32_mac(from_val_fu32) >> 16 );
    to_buffer_fpui8[ 1 ] |= 0x08; // Set EIDE bit 0000 1000
    // from 16 - 24
    to_buffer_fpui8[ 2 ] = (uint8_t) ( GetExdIdfromU32_mac(from_val_fu32) >> 8 );
    // from 24 - 32
    to_buffer_fpui8[ 3 ] = (uint8_t) GetExdIdfromU32_mac( from_val_fu32 );
  }
  return;
}
/**
 *
 * \brief this function is used to copy the user filter data to regular buffer before performing a SPI write
 *
 * \return void
 *
 * \param in from_str_fst contains the filter values
 *
 * \param out regular buffer to be used in the write instruction
 *
 */
VISIBILITY void copy_filter_value_mvd( Boolean is_extended_fbl, uint32_t filter_value_fu32, uint8_t *to_array_fpui8 )
{
  if ( to_array_fpui8 != NULL_PTR ) {
    // Reset the array
    convert_to_mcp_order_mvd( is_extended_fbl , filter_value_fu32 , to_array_fpui8 );
  }
  return;
}

/**
 *
 * \brief function used to copy a uin32_t mask value to an array before SPI write instruction
 *
 * \return void
 *
 * \param in from_mask_value_fui32 mask value
 *
 * \param out to_array_fpui8 destination array
 *
 */
VISIBILITY void copy_mask_value_mvd( Boolean is_extd_fbl, uint32_t from_mask_value_fui32, uint8_t *to_array_fpui8 )
{
  if ( to_array_fpui8 != NULL_PTR ) {
    // Reset the array
    convert_to_mcp_order_mvd( is_extd_fbl , from_mask_value_fui32 , to_array_fpui8 );
  }
  return;
}

/**
 *
 * \brief function used to set the abort tx flag for the corresponding Tx buffer, this function assumes \n
 *        that the buffer number is in range [0..2]
 *
 * \return void
 *
 * \param in Buffer number
 *
 */
VISIBILITY void set_tx_buffer_abort_flag_mvd( TxBufferNbr_gui8_t buffer_nbr_fui8 )
{
  if ( buffer_nbr_fui8 == 0 ) {
    main_flags_mst.abort_tx_buffer0_flag = 1;
  } else if ( buffer_nbr_fui8 == 1 ) {
    main_flags_mst.abort_tx_buffer1_flag = 1;
  } else {
    main_flags_mst.abort_tx_buffer2_flag = 1;
  }
}

/**
 *
 * \brief function used to clear the abort tx flag for the corresponding Tx buffer, this function assumes \n
 *        that the buffer number is in range [0..2]
 *
 * \return void
 *
 * \param in Buffer number
 *
 */
VISIBILITY void clear_tx_buffer_abort_flag_mvd( TxBufferNbr_gui8_t buffer_nbr_fui8 )
{
  if ( buffer_nbr_fui8 == 0 ) {
    main_flags_mst.abort_tx_buffer0_flag = 0;
  } else if ( buffer_nbr_fui8 == 1 ) {
    main_flags_mst.abort_tx_buffer1_flag = 0;
  } else {
    main_flags_mst.abort_tx_buffer2_flag = 0;
  }
}

/**
 * \brief This function will abort the transmission of the given TX_BUFFER id.
 *
 * \return OP_OK if the buffer ID is valid and the SPI transaction performed without errors, OP_NOK instead.
 *
 * \param buffer_nbr_fui8 holds the desired TX buffer ID.
 *
 */
VISIBILITY StdReturn_gui8_t abort_tx_buffer_mui8( TxBufferNbr_gui8_t buffer_nbr_fui8 )
{
  StdReturn_gui8_t ret_val_lui8 = OP_OK;
  uint8_t aux_array_lpui8[ 3 ];

  switch ( buffer_nbr_fui8 ) {
    case 0x00: {
      aux_array_lpui8[ 0 ] = REG_TXB0CTRL_ADDR;
      break;
    }
    case 0x01: {
      aux_array_lpui8[ 0 ] = REG_TXB1CTRL_ADDR;
      break;
    }
    case 0x02: {
      aux_array_lpui8[ 0 ] = REG_TXB2CTRL_ADDR;
      break;
    }
    default:
      ret_val_lui8 = OP_NOK;
  }
  if ( ret_val_lui8 == OP_OK ) {
    aux_array_lpui8[ 1 ] = 0x80; /**< Mask bit 0000 1000*/
    aux_array_lpui8[ 2 ] = 0x80; /**< 3th bit is set to 1*/
    // write SPI data
    ret_val_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , aux_array_lpui8[ 0 ] , &aux_array_lpui8[ 1 ] , 2 );
    if ( ret_val_lui8 == OP_OK ) {
      set_tx_buffer_abort_flag_mvd( buffer_nbr_fui8 );
    }
  } else {
    // Buffer number not valid
  }
  return ret_val_lui8;
}

/**
 *
 * \brief Fill the the given TX buffer with the message that will be sent.
 *
 * \return OP_OK of the pointer parameters are different from NULL_PTR.
 *
 * \param tx_msg_fpst as input, a pointer to a structure that hold the CAN message properties.
 *
 * \param buffer_to_fpui8 as output, a pointer to a buffer that will be written to the MCP2515.
 *
 * \note This function assumes that the buffer_to_fpui8 is initialized to 0.
 *
 */
VISIBILITY StdReturn_gui8_t fill_tx_buffer_mui8( const CanMsg_gst_t * tx_msg_fpst, uint8_t *buffer_to_fpui8 )
{
  StdReturn_gui8_t ret_val_lui8 = OP_OK;
  uint8_t loop_lui8 = 0;

  if ( ( buffer_to_fpui8 != NULL_PTR ) && ( tx_msg_fpst != NULL_PTR ) ) {
    // Set: TXBnSIDH, TXBnSIDL, TXBnEID8 and TXBnEID0
    convert_to_mcp_order_mvd( tx_msg_fpst->is_extd_id_bl , tx_msg_fpst->msg_id_u32 , &buffer_to_fpui8[ 0 ] );
    if ( tx_msg_fpst->frame_type_enm == REMOTE_FRAME_enm ) {
      // Set the frame type
      buffer_to_fpui8[ 4 ] |= 0x40; // set the 6th bit to one, 0100 0000
    }
    // Set TXBnDLC register
    buffer_to_fpui8[ 4 ] |= Min_mac( 8 , tx_msg_fpst->msg_dlc_ui8 );
    // Set TXBnDm registers
    for ( loop_lui8 = 0; loop_lui8 < Min_mac( 8 , tx_msg_fpst->msg_dlc_ui8 ); loop_lui8++ ) {
      buffer_to_fpui8[ 5 + loop_lui8 ] = tx_msg_fpst->data_pui8[ loop_lui8 ];
    }
  } else {
    ret_val_lui8 = OP_NOK;
  }
  return ret_val_lui8;
}

/**
 *
 * \brief Used to get a valid time stamp (a free running timer could be used), resolution tens of microseconds.
 *
 * \return void
 *
 * \param time_stamp_fpst as output pointer to the time stamp structure.
 *
 */
VISIBILITY void get_valid_time_stamp_mvd( TimeStamp_gst_t * time_stamp_fpst )
{
  // push both rx buffer to rx_queue
  if ( device_config_mst.cb_time_stamp_gui8 != NULL_PTR ) {
    (void) GetTimeStamp_mac( time_stamp_fpst );
  } else {
    // TODO trace time stamp callback error
    time_stamp_fpst->time_stamp_hi_u32 = 0;
    time_stamp_fpst->time_stamp_lo_u32 = 0;
  }
  return;
}

/**
 *
 * \brief Write the given TX buffer, and trigger the message transmission.
 *
 * \return OP_OK if the given TX buffer number is correct and the SPI transactions are done correctly.
 *
 * \param tx_msg_fpst as input, pointer to the structure that holds the CAN message properties.
 *
 * \param tx_buffer_nbr_lui8 as input, the desired TX buffer number.
 *
 */
VISIBILITY StdReturn_gui8_t transmit_msg_mui8( const CanMsg_gst_t * tx_msg_fpst, TxBufferNbr_gui8_t tx_buffer_nbr_lui8 )
{
  StdReturn_gui8_t last_return_lui8 = OP_OK;
  uint8_t spi_trans_lpui8[ 15 ];
  GenericReg_mun_t gen_reg_lun;

  if ( tx_msg_fpst != NULL_PTR ) {
    memset( (uint8_t*) spi_trans_lpui8 , 0x00 , sizeof( spi_trans_lpui8 ) );
    switch ( tx_buffer_nbr_lui8 ) {
      case TX_BUFFER_0: {
        spi_trans_lpui8[ 0 ] = REG_TXB0CTRL_ADDR; // TXB0CTRL as a base address 30h
        spi_trans_lpui8[ 1 ] = SPI_LOAD_TXB0_ALL_INST;
        break;
      }
      case TX_BUFFER_1: {
        spi_trans_lpui8[ 0 ] = REG_TXB1CTRL_ADDR; // TXB1CTRL as a base address 40h
        spi_trans_lpui8[ 1 ] = SPI_LOAD_TXB1_ALL_INST;
        break;
      }
      case TX_BUFFER_2: {
        spi_trans_lpui8[ 0 ] = REG_TXB2CTRL_ADDR; // TXB2CTRL as a base address 50h
        spi_trans_lpui8[ 1 ] = SPI_LOAD_TXB2_ALL_INST;
        break;
      }
      default: {
        last_return_lui8 = OP_NOK;
        break;
      }
    }
    if ( last_return_lui8 == OP_OK ) {
      // spi_trans_lpui8[0] holds the base address for write command
      // spi_trans_lpui8[1] holds the base register
      last_return_lui8 = fill_tx_buffer_mui8( tx_msg_fpst , &spi_trans_lpui8[ 2 ] );
      if ( last_return_lui8 == OP_OK ) {
        // First clear the TXBnREQ bit.
        last_return_lui8 = clear_TXBnREQ_mui8( tx_buffer_nbr_lui8 );
        if ( last_return_lui8 == OP_OK ) {
          // SPI LOAD Tx
          last_return_lui8 = SPIWriteBytes_mac( spi_trans_lpui8[ 1 ] , SPI_NO_ADDR , &spi_trans_lpui8[ 2 ] , 13 );
          if ( last_return_lui8 == OP_OK ) {
            // set TXBnCTRL priority and trigger the transmission
            gen_reg_lun.REG_ui8 = 0;
            gen_reg_lun.TXBnCTRL.TXP = tx_msg_fpst->msg_priority_ui8;
            gen_reg_lun.TXBnCTRL.TXREQ = 1;
            last_return_lui8 = SPIWriteBytes_mac( SPI_WRITE_INST , spi_trans_lpui8[ 0 ] , &gen_reg_lun.REG_ui8 , 1 );
          } else {
            // SPI write error
            last_return_lui8 = ERROR_SPI_WRITE_OP;
          }
        } else {
          // Error while clearing TXBnREQ
          last_return_lui8 = ERROR_FLAG_CLR_OP;
        }
      } else {
        // Error while filling the buffer
        last_return_lui8 = ERROR_TXB_FILL_OP;
      }
    }
  } else {
    // pointer is null
    last_return_lui8 = OP_NOK;
  }
  return last_return_lui8;
} // end of transmit_msg_mui8()

/**
 *
 * \brief Clears the TXBnREQ bit, used before filling the TX buffer so the transmission is not
 *        triggered at the same time.
 *
 * \return OP_NOK if the tx buffer is invalid or if there is an error in the SPI transaction.
 *
 * \param tx_buffer_nbr_lui8 as input, the given TX buffer number.
 *
 */
VISIBILITY StdReturn_gui8_t clear_TXBnREQ_mui8( TxBufferNbr_gui8_t tx_buffer_nbr_lui8 )
{
  StdReturn_gui8_t result_lui8 = OP_OK;
  uint8_t spi_trans_lpui8[ 3 ];

  memset( (uint8_t*) spi_trans_lpui8 , (uint8_t) 0 , sizeof( spi_trans_lpui8 ) );
  switch ( tx_buffer_nbr_lui8 ) {
    case TX_BUFFER_0: {
      spi_trans_lpui8[ 0 ] = REG_TXB0CTRL_ADDR; // TXB0CTRL as a base address 30h
      break;
    }
    case TX_BUFFER_1: {
      spi_trans_lpui8[ 0 ] = REG_TXB1CTRL_ADDR; // TXB0CTRL as a base address 40h
      break;
    }
    case TX_BUFFER_2: {
      spi_trans_lpui8[ 0 ] = REG_TXB2CTRL_ADDR; // TXB0CTRL as a base address 50h
      break;
    }
    default: {
      result_lui8 = OP_NOK;
      break;
    }
  }
  if ( result_lui8 == OP_OK ) {
    spi_trans_lpui8[ 1 ] = (uint8_t) 0x08; // Mask value for TXBnREQ bit 0000 1000
    spi_trans_lpui8[ 2 ] = (uint8_t) 0x00; // Clear the TXBnREQ bit
    result_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , spi_trans_lpui8[ 0 ] , &spi_trans_lpui8[ 1 ] , 2 );
  }
  return result_lui8;
}

/**
 *
 * \brief Copy the CAN message data from the given MCP2515 buffer to CAN message structure.
 *
 * \return void
 *
 * \param rx_raw_buffer_fpui8 as input, pointer to the buffer that holds the MCP2515 data buffer.
 *
 * \param msg_fpst as output, the structure to which the data will copied.
 *
 */
VISIBILITY void copy_rx_raw_buffer_mvd( const uint8_t * rx_raw_buffer_fpui8, CanMsg_gst_t *msg_fpst )
{
  uint8_t loop_index_ui8 = 0;
  if ( ( rx_raw_buffer_fpui8 != NULL_PTR ) && ( msg_fpst != NULL_PTR ) ) {
    msg_fpst->msg_id_u32 = (uint32_t) ( rx_raw_buffer_fpui8[ 0 ] << 3 ); /**< Fill SID */
    msg_fpst->msg_id_u32 |= (uint32_t) ( rx_raw_buffer_fpui8[ 1 ] >> 5 );
    msg_fpst->is_extd_id_bl = (Boolean) ( ( rx_raw_buffer_fpui8[ 1 ] & 0x08 ) >> 3 );
    if ( msg_fpst->is_extd_id_bl == TRUE ) {
      // Copy the extended part
      msg_fpst->msg_id_u32 |= (uint32_t) ( ( rx_raw_buffer_fpui8[ 1 ] & 0x03 ) << 27 );
      msg_fpst->msg_id_u32 |= (uint32_t) ( rx_raw_buffer_fpui8[ 2 ] << 19 );
      msg_fpst->msg_id_u32 |= (uint32_t) ( rx_raw_buffer_fpui8[ 3 ] << 11 );
      // Extended remote or extended data
      msg_fpst->frame_type_enm = (FrameType_genm_t) ( ( rx_raw_buffer_fpui8[ 4 ] & 0x40 ) >> 6 );
    } else {
      // Standard remote or standard data
      msg_fpst->frame_type_enm = (FrameType_genm_t) ( ( rx_raw_buffer_fpui8[ 1 ] & 0x10 ) >> 4 );
    }
    msg_fpst->msg_dlc_ui8 = rx_raw_buffer_fpui8[ 4 ] & 0x0F;
    // Normalize the data length
    if ( msg_fpst->msg_dlc_ui8 > 8 ) {
      msg_fpst->msg_dlc_ui8 = 8;
    }
    for ( loop_index_ui8 = 0; loop_index_ui8 < msg_fpst->msg_dlc_ui8; loop_index_ui8++ ) {
      msg_fpst->data_pui8[ loop_index_ui8 ] = rx_raw_buffer_fpui8[ 5 + loop_index_ui8 ];
    }
  }
  return;
}

/**
 *
 * \brief read_rx_buffer_mui8 function used to the specific buffer, and put all the element to message
 *        container.
 *
 * \note At the end of execution the RXBnREQ will be cleared automatically since the Read RX buffer
 *       instruction is used to read the buffer contents
 *
 * \return OP_OK if read operation end successfully, OP_NOK if not.
 *
 * \param buffer_nbr_fui8 as input, hold the buffer number to be read.
 *
 * \param msg_fpst pointer as output, will hold the message context and data
 *
 */
VISIBILITY StdReturn_gui8_t read_rx_buffer_mui8( RxBufferNbr_gui8_t buffer_nbr_fui8, CanMsg_gst_t *msg_fpst )
{
  StdReturn_gui8_t ret_val_lui8 = OP_OK;
  uint8_t spi_transaction_lpui8[ 14 ];

  if ( msg_fpst != NULL_PTR ) {
    memset( (uint8_t*) spi_transaction_lpui8 , 0x00 , sizeof( spi_transaction_lpui8 ) );
    if ( buffer_nbr_fui8 == RX_BUFFER_0 ) {
      spi_transaction_lpui8[ 0 ] = SPI_READ_RXB0_HEADER_INST;
    } else if ( buffer_nbr_fui8 == RX_BUFFER_1 ) {
      spi_transaction_lpui8[ 0 ] = SPI_READ_RXB1_HEADER_INST;
    } else {
      ret_val_lui8 = OP_NOK;
    }
    if ( ret_val_lui8 == OP_OK ) {
      // Buffer number is correct
      ret_val_lui8 = SPIWriteBytes_mac( spi_transaction_lpui8[ 0 ] , SPI_NO_ADDR , &spi_transaction_lpui8[ 1 ] , 13 );
      if ( ret_val_lui8 == OP_OK ) {
        // Copy RAW buffer to message structure.
        copy_rx_raw_buffer_mvd( &spi_transaction_lpui8[ 1 ] , msg_fpst );
      }
    }
  } else {
    // Null pointer
    ret_val_lui8 = OP_NOK;
  }
  return ret_val_lui8;
}

/**
 *
 * \brief Clear a specific CANINTF flag.
 *
 * \return OP_OK if the given flag is correct and the SPI transaction is successfully done.
 *
 * \param flag_fenm as input, holds the desired flag.
 *
 */
VISIBILITY StdReturn_gui8_t clear_CANINT_flag_mui8( CanIntFlags_menm_t flag_fenm )
{
  StdReturn_gui8_t ret_value_lui8 = OP_NOK;
  uint8_t spi_trans_pui8[ 2 ];

  memset( spi_trans_pui8 , 0x00 , sizeof( spi_trans_pui8 ) );
  // Set the value mask
  if ( ( flag_fenm >= 0 ) && ( flag_fenm < CANINT_NUMBER_OF_FLAGS_enm ) ) {
    // Create the Mask
    spi_trans_pui8[ 0 ] = (uint8_t) ( 1 << (uint8_t) flag_fenm );
    // Set the value to 0
    spi_trans_pui8[ 1 ] = 0;
    ret_value_lui8 = SPIWriteBytes_mac( SPI_BIT_MODIFY_INST , REG_CANINTF_ADDR , &spi_trans_pui8[ 0 ] , 2 );
  }
  return ret_value_lui8;
}
// end of file mcp2515.c
