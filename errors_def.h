/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 *
 * \file errors_def.h
 *
 * \date 2020-03-20 00:19:27
 *
 * \author Fahd Tauil (tauil.fahd@gmail.com)
 *
 * \brief
 *
 */

#ifndef ERRORS_DEF_H_
#define ERRORS_DEF_H_


#define ERROR_SPI_WRITE_OP         ((uint8_t)0x02)
#define ERROR_SPI_READ_OP          ((uint8_t)0x03)
#define ERROR_RXB0_SET_OP          ((uint8_t)0x04)
#define ERROR_RXB1_SET_OP          ((uint8_t)0x05)
#define ERROR_QUEUE_CREATE_OP      ((uint8_t)0x06)
#define ERROR_STATUS_READ_OP       ((uint8_t)0x07)
#define ERROR_RXB0_READ_OP         ((uint8_t)0x08)
#define ERROR_RXB1_READ_OP         ((uint8_t)0x09)
#define ERROR_FLAG_CLR_OP          ((uint8_t)0x0a)
#define ERROR_QUEUE_PUSH_OP        ((uint8_t)0x0b)
#define ERROR_QUEUE_POP_OP         ((uint8_t)0x0c)
#define ERROR_TXB0_WRITE_OP        ((uint8_t)0x0d)
#define ERROR_TXB1_WRITE_OP        ((uint8_t)0x0e)
#define ERROR_TXB2_WRITE_OP        ((uint8_t)0x0f)
#define ERROR_SET_MODE_OP          ((uint8_t)0x10)
#define ERROR_TXB_FILL_OP          ((uint8_t)0x11)
#define ERROR_OPMODE_SET_OP        ((uint8_t)0x12)
#define ERROR_RESET_REQ_OP         ((uint8_t)0x13)
#define ERROR_NULL_PTR             ((uint8_t)0x14)

#endif /* ERRORS_DEF_H_ */
